from pprint import pprint

def is_in_danger(board, row_index, column_index):
    # Check row for a queen
    for index, value in enumerate(board[row_index]):
        if value == 0 or index == column_index:
            pass
        else:
            return True
    #Check diagonal for a queen
    for index, value in enumerate(board[row_index]):
        if row_index > 1:
            pass


def place_queen_in_column(board, column_number):
    if column_number >= len(board):
        return
    for row_number in range(len(board)):
        board[row_number][column_number] = 1
        if is_in_danger(board, row_number, column_number):
            board[row_number][column_number] = 0
        else:
            place_queen_in_column(board, column_number + 1)


board = []
def n_queens(n):
    for i in range(1,int(n+1)):
        row = []
        for j in range(1,int(n+1)):
            row.append(0)
        board.append(row)
    return board



# if __name__ == "main":
board = n_queens(4)
pprint(board, width =40)
print(len(board))